package com.example.sourcessinksanalysis;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.AppBarConfiguration;
import com.example.sourcessinksanalysis.databinding.ActivityMainBinding;



public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                macAddressToIntent();
            }
        });

        binding.button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btAddressToIntent();
            }
        });


    }

    /**
     * @source macAddress <android.net.wifi.WifiInfo: java.lang.String getMacAddress()>
     * @sink sendIntent <android.content.Intent: android.content.Intent putExtra(java.lang.String,java.lang.String)>
     *      {
     *          declaringClass = "android/content/Intent",
     *          methods = [
     *              {signature = "putExtra(java.lang.String,java.lang.String)", params = [1]},
     *          ]
     *      }
     */
    private void macAddressToIntent() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        WifiManager wifiManager = (WifiManager) getBaseContext().getSystemService(Context.WIFI_SERVICE);
        String macAddress = wifiManager.getConnectionInfo().getMacAddress();

//        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//
//        WifiInfo wifiInfo = (WifiInfo) connectivityManager
//                .getNetworkCapabilities(connectivityManager.getActiveNetwork())
//                .getTransportInfo();
//        String macAddress = wifiInfo.getMacAddress();

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, macAddress);
        sendIntent.setType("text/plain");
        // Try to invoke the intent.
        try {
            startActivity(sendIntent);
        } catch (ActivityNotFoundException e) {

        }
    }

    /**
     * @source btAddress <android.bluetooth.BluetoothAdapter: java.lang.String getAddress()>
     * @sink sendIntent <android.content.Intent: android.content.Intent putExtra(java.lang.String,java.lang.String)>
     *      {
     *          declaringClass = "android/content/Intent",
     *          methods = [
     *              {signature = "putExtra(java.lang.String,java.lang.String)", params = [1]},
     *          ]
     *      }
     */
    private void btAddressToIntent() {
        BluetoothManager btManager = (BluetoothManager) getBaseContext().getSystemService(Context.BLUETOOTH_SERVICE);
        String btAddress = btManager.getAdapter().getAddress();

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, btAddress);
        sendIntent.setType("text/plain");
        // Try to invoke the intent.
        try {
            startActivity(sendIntent);
        } catch (ActivityNotFoundException e) {

        }
    }

}